import 'package:cronotimer/painters/TimerPainter.dart';
import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';
import 'dart:math' as math;

class TimerPage extends StatefulWidget {
  @override
  _TimerPageState createState() => _TimerPageState();
}

class _TimerPageState extends State<TimerPage> with TickerProviderStateMixin {
  AnimationController controller;

  int counter = 0;
  int minutes = 0;
  int seconds = 0;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      duration: Duration(seconds: 60),
      vsync: this,
    );
  }

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);

    return Scaffold(
        body: Padding(
            padding: EdgeInsets.only(
                left: 15.0,
                top: 80.0,
                right: 15.0,
                bottom: 15.0
            ),
            child: SafeArea(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      "Sets: $counter",
                      style: TextStyle(
                        fontSize: 34
                      ),
                    ),
                    Expanded(
                        child: Align(
                            alignment: FractionalOffset.center,
                            child: AspectRatio(
                                aspectRatio: 1.0,
                                child: Stack(
                                    children: <Widget>[
                                      Positioned.fill(
                                          child: AnimatedBuilder(
                                              animation: controller,
                                              builder: (BuildContext context, Widget child) {
                                                return new CustomPaint(
                                                    painter: TimerPainter(
                                                      animation: controller,
                                                      backgroundColor: Colors.blueGrey,
                                                      color: themeData.indicatorColor,
                                                    )
                                                );
                                              }
                                          )
                                      ),
                                      Align(
                                          alignment: FractionalOffset.center,
                                          child: Column(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: <Widget>[
                                                Row(
                                                    crossAxisAlignment: CrossAxisAlignment.end,
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    children: <Widget>[
                                                      NumberPicker.integer(
                                                          initialValue: this.seconds,
                                                          minValue: 0,
                                                          maxValue: 59,

                                                          highlightSelectedValue: true,
                                                          listViewWidth: 150,
                                                          itemExtent: 120,
                                                          onChanged: (newValue) =>
                                                            setState(() =>
                                                              this.seconds = newValue
                                                            )
                                                      ),
                                                      Padding(
                                                        padding: EdgeInsets.only(bottom: 40,right: 15),
                                                        child: Text(
                                                          ":",
                                                          style: TextStyle(
                                                            fontSize: 100
                                                          )
                                                        ),
                                                      ),
                                                      NumberPicker.integer(
                                                          initialValue: this.minutes,
                                                          minValue: 0,
                                                          maxValue: 59,
                                                          highlightSelectedValue: true,
                                                          listViewWidth: 150,
                                                          itemExtent: 120,
                                                          step: 10,
                                                          onChanged: (newValue) =>
                                                              setState(() =>
                                                              this.minutes = newValue
                                                              )
                                                      ),
                                                    ]
                                                ),
                                              ]
                                          )
                                      ),
                                  ]
                                )
                            )
                        )
                    ),
                    Container(
                        margin: EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            FloatingActionButton(
                                child: AnimatedBuilder(
                                    animation: controller,
                                    builder: (BuildContext context, Widget child) {
                                      return Icon(
                                          controller.isAnimating ? Icons.stop : Icons.play_arrow
                                      );
                                    }
                                ),
                                onPressed: () {
                                  if (controller.isAnimating)
                                    controller.reset();
                                  else {
                                    incrementCounter();
                                    controller.reverse(
                                        from: controller.value == 0.0
                                            ? 1.0
                                            : controller.value);
                                  }
                            }),
                            FloatingActionButton(
                              child: AnimatedBuilder(
                                  animation: controller,
                                  builder: (BuildContext context, Widget child) {
                                    return Icon(Icons.refresh);
                                  }),
                              onPressed: () {
                                resetCounter();
                                controller.reset();
                              },
                        )
                      ])
                    )
              ]),
            )
        )
    );
  }

  void incrementCounter() {
    setState(() {
      counter++;
    });
  }

  void resetCounter() {
    setState(() {
      counter = 0;
    });
  }

  String get timerString {
    Duration duration = (controller.value == 0)
        ? controller.duration
        : controller.duration * controller.value;
    return '${duration.inMinutes}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
  }
}
