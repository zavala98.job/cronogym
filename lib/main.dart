import 'package:cronotimer/pages/TimerPage.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: TimerPage(),
        theme: ThemeData(
          canvasColor: Colors.indigo,
          iconTheme: IconThemeData(
            color: Colors.black54,
          ),
          accentColor: Colors.black54,
          brightness: Brightness.dark,
          textTheme: TextTheme(
            body1: TextStyle(
              fontSize: 20
            )
          )
        ));
  }
}